<div class="title grid grid-cols-1 justify-items-center text-white m-4">
    <div class="title-text flex flex-col space-y-2 justify-center py-2 px-2 text-center">
        <p class="font-bold text-xl">MASSIVE CONFERENCE IN </p>
        <p class="font-bold text-2xl">LONDON</p>
        <p class="italic">Dont miss out. Register today and discover our latest offers</p>
        <p class="italic">june 11 2017</p>
        <p class="text-2xl">00 days 00:00:00</p> 
    </div>
</div>