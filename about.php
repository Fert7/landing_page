    <div class="absolute lg:top-2/3 md:top-5/6 bg-white grid grid-cols-1 rounded mx-4 ">
        <!-- SERVICES -->
        <div class="zone1">
            <div class="content lg:grid grid-cols-3 lg:gap-4 md:grid grid-cols-2 md:gap-2 sm:grid grid-cols-1 sm:gap-2 justify-items-center bg-gray-900 text-white rounded md:h-full">

                <div class="element flex-col py-2">
                    <div class="pl-8"> Next conference Registration Open </div>
                    <div class="flex flex-row">
                        <div>
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-12 w-24" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z" />
                            </svg>
                        </div>
                        <div class="flex flex-col">
                            <p>Date <br> March 17th 2017</p>
                        </div>
                    </div>

                </div>

                <div class="element flex-col py-4">

                    <div class="flex flex-row">
                        <div>
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-12 w-24" viewBox="0 0 20 20" fill="currentColor">
                                <path fill-rule="evenodd" d="M5.05 4.05a7 7 0 119.9 9.9L10 18.9l-4.95-4.95a7 7 0 010-9.9zM10 11a2 2 0 100-4 2 2 0 000 4z" clip-rule="evenodd" />
                            </svg>
                        </div>
                        <div class="flex flex-col">
                            <p>Location <br> Oxford Street,London,UK</p>
                        </div>
                    </div>

                </div>

                <div class="element flex flex-col py-4">
                    
                    <div class="flex flex-row">
                        <div>
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-12 w-24" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z" />
                            </svg>
                        </div>
                        <div class="flex flex-col">
                            <p>Time <br>09:45am-10:15pm</p>
                        </div>
                    </div>

                </div>

            </div>
        </div>

        <!--  -->

        <div class="zone2 py-8">
            <div class="informations grid grid-cols-1">
                <div class="informations1">
                    <div class="text-center">
                        <div class="m-4 text-2xl font-bold">
                            About This Event
                        </div>
                        <div class="m-4 text-xl">
                            CREATIVE EXPERTS MAKING GREAT IDEAS HAPPEN
                        </div>
                        <div class="m-4 px-8 text-gray-500">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                            when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                        </dive>
                    </div>
                </div>
            </div>

            <div class="information2 lg:grid grid-cols-3 gap-4 mx-4 sm:flex-col sm:justify-center sm:text-center">

                <div class="flex flex-col sm:items-center space-y-2 px-8">
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 text-purple-800" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 20l4-16m4 4l4 4-4 4M6 16l-4-4 4-4" />
                        </svg>
                    </div>
                    <div class="text-purple-800">Better Coding</div>
                    <div class="h-24">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                    </div>
                </div>

                <div class="flex flex-col sm:items-center space-y-2 px-8">
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 text-purple-800" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 7h8m0 0v8m0-8l-8 8-4-4-6 6" />
                        </svg>
                    </div>
                    <div class="text-purple-800">Secret to Succeess</div>
                    <div class="h-24">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                    </div>
                </div>

                <div class="flex flex-col sm:items-center space-y-2 px-8">
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 text-purple-800" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17.657 18.657A8 8 0 016.343 7.343S7 9 9 10c0-2 .5-5 2.986-7C14 5 16.09 5.777 17.656 7.343A7.975 7.975 0 0120 13a7.975 7.975 0 01-2.343 5.657z" />
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9.879 16.121A3 3 0 1012.015 11L11 14H9c0 .768.293 1.536.879 2.121z" />
                        </svg>
                    </div>
                    <div class="text-purple-800">Make Ideas</div>
                    <div class="h-24">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                    </div>
                </div>
            </div>
        </div>

        <!--  -->
        
       <div class="zone4">
            <div class="informations3 grid grid-cols-1 gap-4 justify-items-center mx-4  py-36">
                <div class="bg-gray-200 p-4">
                <div class="flex items-center justify-center ">
                    <div class="flex flex-row space-x-2 border-2 border-gray-200 rounded">
                        <input type="text" class="px-4 py-2 w-80" placeholder="ENTER YOUR EMAIL ADDRESS">
                        <button class="px-4 text-white bg-purple-600 border-l ">
                            GET STARTED
                        </button>
                    </div>
                </div>
                </div>
            </div>
       </div>

        <div class="zone5 grid grid-cols-1 gap-4 mx-8 text-center">
            <div class="font-bold text-2xl text-bold">
                Let's talk Product
            </div>
            <div class="p-4">
                Lorem Ipsum est simplement un texte factice de l'industrie de l'impression et de la composition. 
                Lorem Ipsum est le texte factice standard de l'industrie depuis les années 1500, lorsqu'un imprimeur 
                inconnu a pris une galère de caractères et l'a brouillé pour en faire un livre spécimen de caractères.
                Lorem Ipsum est le texte factice standard de l'industrie depuis les années 1500, lorsqu'un imprimeur 
                inconnu a pris une galère de caractères et l'a brouillé pour en faire un livre spécimen de caractères. 
            </div>
        </div>

        <div class="zone6 lg:grid grid-cols-3 sm:flex-col  gap-4 mx-4">

            <div class="flex flex-col space-y-2 justify-center items-center text-center ">
                <div>
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-12 w-12 text-purple-800" viewBox="0 0 20 20" fill="currentColor">
                        <path fill-rule="evenodd" d="M18 5v8a2 2 0 01-2 2h-5l-5 4v-4H4a2 2 0 01-2-2V5a2 2 0 012-2h12a2 2 0 012 2zM7 8H5v2h2V8zm2 0h2v2H9V8zm6 0h-2v2h2V8z" clip-rule="evenodd" />
                    </svg>
                </div>
                <div class="font-bold">First Feature</div>
                <div>Lorem Ipsum est simplement un texte factice de l'industrie de l'impression et de la composition.</div>
            </div>

            <div class="flex flex-col space-y-2 justify-center items-center text-center ">
                <div>
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-12 w-12 text-green-800" viewBox="0 0 20 20" fill="currentColor">
                        <path fill-rule="evenodd" d="M2.166 4.999A11.954 11.954 0 0010 1.944 11.954 11.954 0 0017.834 5c.11.65.166 1.32.166 2.001 0 5.225-3.34 9.67-8 11.317C5.34 16.67 2 12.225 2 7c0-.682.057-1.35.166-2.001zm11.541 3.708a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd" />
                    </svg>
                </div>
                <div class="font-bold">Second Feature</div>
                <div>Lorem Ipsum est simplement un texte factice de l'industrie de l'impression et de la composition.</div>
            </div>

            <div class="flex flex-col space-y-2 justify-center items-center text-center">
                <div>
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-12 w-12 text-red-800" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 11c0 3.517-1.009 6.799-2.753 9.571m-3.44-2.04l.054-.09A13.916 13.916 0 008 11a4 4 0 118 0c0 1.017-.07 2.019-.203 3m-2.118 6.844A21.88 21.88 0 0015.171 17m3.839 1.132c.645-2.266.99-4.659.99-7.132A8 8 0 008 4.07M3 15.364c.64-1.319 1-2.8 1-4.364 0-1.457.39-2.823 1.07-4" />
                    </svg>
                </div>
                <div class="font-bold">Third Feature</div>
                <div>Lorem Ipsum est simplement un texte factice de l'industrie de l'impression et de la composition.</div>
            </div>
        </div>


        <div class="zone7 flex flex-col space-y-6 justify-center items-center text-center py-36 px-8">
            <div class="font-bold text-2xl">
                Who's Speacking ?
            </div>
            <div>
                Where the business work meet ?
            </div>
            <div class="lg:grid grid-cols-4 gap-8 sm:flex-col sm:space-y-6 justify-items-center items-center">
                <div class="flex flex-col items-center space-y-4">
                    <div><img class="card h-24 w-24" src="images/images.jpeg" alt=""></div>
                    <div class="font-bold">Gigi Hadid</div>
                    <div>Designer</div>
                    <div>Lorem Ipsum est simplement un texte factice de l'industrie de l'impression et de la composition.</div>
                    <div class="flex flex-row space-x-2">

                        <div>
                            <svg class="w-6 h-6 fill-current"xmlns="http://www.w3.org/2000/svg"viewBox="0 0 24 24">
                                <path d="M23.953 4.57a10 10 0 01-2.825.775 4.958 4.958 0 002.163-2.723c-.951.555-2.005.959-3.127 1.184a4.92 4.92 0 00-8.384 4.482C7.69 8.095 4.067 6.13 1.64 3.162a4.822 4.822 0 00-.666 2.475c0 1.71.87 3.213 2.188 4.096a4.904 4.904 0 01-2.228-.616v.06a4.923 4.923 0 003.946 4.827 4.996 4.996 0 01-2.212.085 4.936 4.936 0 004.604 3.417 9.867 9.867 0 01-6.102 2.105c-.39 0-.779-.023-1.17-.067a13.995 13.995 0 007.557 2.209c9.053 0 13.998-7.496 13.998-13.985 0-.21 0-.42-.015-.63A9.935 9.935 0 0024 4.59z"/>
                        </svg>

                        </div>

                        <div>
                            <svg class="w-6 h-6  fill-current"xmlns="http://www.w3.org/2000/svg"viewBox="0 0 24 24">
                                <path d="M24 12.073c0-6.627-5.373-12-12-12s-12 5.373-12 12c0 5.99 4.388 10.954 10.125 11.854v-8.385H7.078v-3.47h3.047V9.43c0-3.007 1.792-4.669 4.533-4.669 1.312 0 2.686.235 2.686.235v2.953H15.83c-1.491 0-1.956.925-1.956 1.874v2.25h3.328l-.532 3.47h-2.796v8.385C19.612 23.027 24 18.062 24 12.073z"/>
                            </svg>
                        </div>

                        <div>
                            <svg class="w-6 h-6 fill-current" xmlns="http://www.w3.org/2000/svg"viewBox="0 0 448 512">
                                <path d="M100.28 448H7.4V148.9h92.88zM53.79 108.1C24.09 108.1 0 83.5 0 53.8a53.79 53.79 0 0 1 107.58 0c0 29.7-24.1 54.3-53.79 54.3zM447.9 448h-92.68V302.4c0-34.7-.7-79.2-48.29-79.2-48.29 0-55.69 37.7-55.69 76.7V448h-92.78V148.9h89.08v40.8h1.3c12.4-23.5 42.69-48.3 87.88-48.3 94 0 111.28 61.9 111.28 142.3V448z"></path>
                            </svg>
                        </div>

                    </div>
                </div>
                <div class="flex flex-col space-y-4 items-center">
                    <div class="card h-24 w-24"><img class="card h-24 w-24" src="images/téléchargement (1).jpg" alt=""></div>
                    <div class="font-bold">Christian Louboutin</div>
                    <div>Developper</div>
                    <div>Lorem Ipsum est simplement un texte factice de l'industrie de l'impression et de la composition.</div>
                    <div class="flex flex-row space-x-2">
                        <div>
                            <svg class="w-6 h-6  fill-current"xmlns="http://www.w3.org/2000/svg"viewBox="0 0 24 24">
                                <path d="M24 12.073c0-6.627-5.373-12-12-12s-12 5.373-12 12c0 5.99 4.388 10.954 10.125 11.854v-8.385H7.078v-3.47h3.047V9.43c0-3.007 1.792-4.669 4.533-4.669 1.312 0 2.686.235 2.686.235v2.953H15.83c-1.491 0-1.956.925-1.956 1.874v2.25h3.328l-.532 3.47h-2.796v8.385C19.612 23.027 24 18.062 24 12.073z"/>
                            </svg>
                        </div>

                        <div>
                            <svg class="w-6 h-6 fill-current" xmlns="http://www.w3.org/2000/svg"viewBox="0 0 448 512">
                                <path d="M100.28 448H7.4V148.9h92.88zM53.79 108.1C24.09 108.1 0 83.5 0 53.8a53.79 53.79 0 0 1 107.58 0c0 29.7-24.1 54.3-53.79 54.3zM447.9 448h-92.68V302.4c0-34.7-.7-79.2-48.29-79.2-48.29 0-55.69 37.7-55.69 76.7V448h-92.78V148.9h89.08v40.8h1.3c12.4-23.5 42.69-48.3 87.88-48.3 94 0 111.28 61.9 111.28 142.3V448z"></path>
                            </svg>
                        </div>

                    </div>
                </div>
                <div class="flex flex-col space-y-4 items-center">
                    <div class="card h-24 w-24"><img class="card h-24 w-24" src="images/téléchargement (2).jpg" alt=""></div>
                    <div class="font-bold">Kendar Jenner</div>
                    <div>UI/UX</div>
                    <div>Lorem Ipsum est simplement un texte factice de l'industrie de l'impression et de la composition.</div>
                    <div>
                    <div class="flex flex-row space-x-2">

                        <div>
                            <svg class="w-6 h-6 fill-current"xmlns="http://www.w3.org/2000/svg"viewBox="0 0 24 24">
                                <path d="M23.953 4.57a10 10 0 01-2.825.775 4.958 4.958 0 002.163-2.723c-.951.555-2.005.959-3.127 1.184a4.92 4.92 0 00-8.384 4.482C7.69 8.095 4.067 6.13 1.64 3.162a4.822 4.822 0 00-.666 2.475c0 1.71.87 3.213 2.188 4.096a4.904 4.904 0 01-2.228-.616v.06a4.923 4.923 0 003.946 4.827 4.996 4.996 0 01-2.212.085 4.936 4.936 0 004.604 3.417 9.867 9.867 0 01-6.102 2.105c-.39 0-.779-.023-1.17-.067a13.995 13.995 0 007.557 2.209c9.053 0 13.998-7.496 13.998-13.985 0-.21 0-.42-.015-.63A9.935 9.935 0 0024 4.59z"/>
                        </svg>

                        </div>

                        <div>
                            <svg class="w-6 h-6  fill-current"xmlns="http://www.w3.org/2000/svg"viewBox="0 0 24 24">
                                <path d="M24 12.073c0-6.627-5.373-12-12-12s-12 5.373-12 12c0 5.99 4.388 10.954 10.125 11.854v-8.385H7.078v-3.47h3.047V9.43c0-3.007 1.792-4.669 4.533-4.669 1.312 0 2.686.235 2.686.235v2.953H15.83c-1.491 0-1.956.925-1.956 1.874v2.25h3.328l-.532 3.47h-2.796v8.385C19.612 23.027 24 18.062 24 12.073z"/>
                            </svg>
                        </div>

                        <div>
                            <svg class="w-6 h-6 fill-current" xmlns="http://www.w3.org/2000/svg"viewBox="0 0 448 512">
                                <path d="M100.28 448H7.4V148.9h92.88zM53.79 108.1C24.09 108.1 0 83.5 0 53.8a53.79 53.79 0 0 1 107.58 0c0 29.7-24.1 54.3-53.79 54.3zM447.9 448h-92.68V302.4c0-34.7-.7-79.2-48.29-79.2-48.29 0-55.69 37.7-55.69 76.7V448h-92.78V148.9h89.08v40.8h1.3c12.4-23.5 42.69-48.3 87.88-48.3 94 0 111.28 61.9 111.28 142.3V448z"></path>
                            </svg>
                        </div>

                        </div>
                    </div>
                </div>
                <div class="flex flex-col space-y-4 items-center">
                    <div class="card h-24 w-24"><img class="card h-24 w-24" src="images/téléchargement.jpg" alt=""></div>
                    <div>Alicia Mopez</div>
                    <div>Community Manager</div>
                    <div>Lorem Ipsum est simplement un texte factice de l'industrie de l'impression et de la composition.</div>
                    <div>
                    <div class="flex flex-row space-x-2">
                        <div>
                            <svg class="w-6 h-6 fill-current"xmlns="http://www.w3.org/2000/svg"viewBox="0 0 24 24">
                                <path d="M23.953 4.57a10 10 0 01-2.825.775 4.958 4.958 0 002.163-2.723c-.951.555-2.005.959-3.127 1.184a4.92 4.92 0 00-8.384 4.482C7.69 8.095 4.067 6.13 1.64 3.162a4.822 4.822 0 00-.666 2.475c0 1.71.87 3.213 2.188 4.096a4.904 4.904 0 01-2.228-.616v.06a4.923 4.923 0 003.946 4.827 4.996 4.996 0 01-2.212.085 4.936 4.936 0 004.604 3.417 9.867 9.867 0 01-6.102 2.105c-.39 0-.779-.023-1.17-.067a13.995 13.995 0 007.557 2.209c9.053 0 13.998-7.496 13.998-13.985 0-.21 0-.42-.015-.63A9.935 9.935 0 0024 4.59z"/>
                        </svg>

                        </div>

                        <div>
                            <svg class="w-6 h-6  fill-current"xmlns="http://www.w3.org/2000/svg"viewBox="0 0 24 24">
                                <path d="M24 12.073c0-6.627-5.373-12-12-12s-12 5.373-12 12c0 5.99 4.388 10.954 10.125 11.854v-8.385H7.078v-3.47h3.047V9.43c0-3.007 1.792-4.669 4.533-4.669 1.312 0 2.686.235 2.686.235v2.953H15.83c-1.491 0-1.956.925-1.956 1.874v2.25h3.328l-.532 3.47h-2.796v8.385C19.612 23.027 24 18.062 24 12.073z"/>
                            </svg>
                        </div>

                        <div>
                            <svg class="w-6 h-6 fill-current" xmlns="http://www.w3.org/2000/svg"viewBox="0 0 448 512">
                                <path d="M100.28 448H7.4V148.9h92.88zM53.79 108.1C24.09 108.1 0 83.5 0 53.8a53.79 53.79 0 0 1 107.58 0c0 29.7-24.1 54.3-53.79 54.3zM447.9 448h-92.68V302.4c0-34.7-.7-79.2-48.29-79.2-48.29 0-55.69 37.7-55.69 76.7V448h-92.78V148.9h89.08v40.8h1.3c12.4-23.5 42.69-48.3 87.88-48.3 94 0 111.28 61.9 111.28 142.3V448z"></path>
                            </svg>
                        </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="zone8 py-12">
            <div class="content grid grid-cols-2 gap-4 justify-items-center bg-gray-900 text-white rounded">
                <div class="flex flex-col space-y-2 justify-start py-8 px-8">
                    <div class="font-bold">Are you Ready Join our Program Meet ?</div>
                    <div>Lorem Ipsum est simplement un texte factice de l'industrie de l'impression et de la composition</div>
                </div>
                <div class="flex justify-end ">
                    <button class="bg-purple-800 rounded text-white mx-12 my-12 px-2">
                        KNOWN MORE
                    </button>
                </div>
            </div>
        </div>

        <div class="zone9 py-12 flex flex-col justify-center items-center space-y-4">
            <div class="font-bold text-2xl">Buy A Ticket and Join Now</div>
            <div>Lorem Ipsum est simplement un texte factice de l'industrie de l'impression et de la composition</div>
            <div class="flex lg:flex-row space-x-2 sm:flex-col sm:space-y-4">
                <div class="w-full max-w-sm">
                    <form class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">

                        <div>
                            <input class="border-b-2 rounded w-full py-6 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="username" type="text" placeholder="Full Name">
                        </div>
                        <div>
                            <input class="border-b-2 rounded w-full py-6 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="email" type="email" placeholder="Email">
                        </div>
                        <div>
                            <input class="border-b-2 rounded w-full py-6 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="telephone" type="telephone" placeholder="Phone No">
                        </div>
                        <div>
                            <input class="border-b-2 rounded w-full py-6 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="number" type="number" placeholder="Number & Ticket">
                        </div>

                        <div class="py-2">
                            <p class="text-gray-500 text-basis py-2">Subscribe for newsletter.</p>
                            <input type="checkbox" class="py-2"> <span class="text-sm text-gray-500">Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati debitis laborio.</span>
                        </div>
                        <div>
                        </div>

                        <div class="flex items-center justify-between">
                            <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="button">
                                RESERVE SEAT
                            </button>
                            
                        </div>
                    </form>
                </div>

                <div class="flex flex-col space-y-2 py-2">
                    <div class="w-full max-w-lg">
                        <div class="bg-white shadow-xl rounded border-2">
                            <div class="flex flex-col justify-center">
                                <div class="flex flex-row">
                                    <div class="flex flex-col border-r-2 px-4 py-2">
                                        <p class="text-2xl font-bold">490</p>
                                        <p>100 places</p>
                                    </div>
                                    <div class="flex flex-col px-4 py-2">
                                        <div class="text-xl font-bold">VIP Pass</div>
                                        <div class="flex flex-row space-x-4">
                                            <div class="flex flex-col">
                                                <p>Lorem adipisicing </p>
                                                <p>Lorem adipisicing </p>
                                            </div>
                                            <div class="flex flex-col">
                                                <p>Lorem adipisicing </p>
                                                <p>Lorem adipisicing </p>
                                            </div>
                                        </div>
                                        <button class="font-bold text-left">Buy Now</button>
                                    </div>

                                    
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="w-full max-w-lg max-h-sm">
                        <div class="bg-white shadow-xl rounded border-2">
                            <div class="flex flex-col justify-center">
                                <div class="flex flex-row">
                                    <div class="flex flex-col border-r-2 px-4 py-2">
                                        <p class="text-2xl font-bold">390</p>
                                        <p>100 places</p>
                                    </div>
                                    <div class="flex flex-col px-4 py-2">
                                        <div class="text-xl font-bold">GOLD Pass</div>
                                        <div class="flex flex-row space-x-4">
                                            <div class="flex flex-col">
                                                <p>Lorem adipisicing </p>
                                                <p>Lorem adipisicing </p>
                                            </div>
                                            <div class="flex flex-col">
                                                <p>Lorem adipisicing </p>
                                                <p>Lorem adipisicing </p>
                                            </div>
                                        </div>
                                        <button class="font-bold text-left">Buy Now</button>
                                    </div>    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="w-full max-w-lg">
                    <div class="bg-white shadow-xl rounded border-2">
                        <div class="flex flex-col justify-center">
                            <div class="flex flex-row">
                                <div class="flex flex-col border-r-2 px-4 py-2">
                                    <p class="text-2xl font-bold">290</p>
                                    <p>100 places</p>
                                </div>
                                <div class="flex flex-col px-4 py-2">
                                    <div class="text-xl font-bold">SILVER Pass</div>
                                    <div class="flex flex-row space-x-4">
                                        <div class="flex flex-col">
                                            <p>Lorem adipisicing </p>
                                            <p>Lorem adipisicing </p>
                                        </div>
                                        <div class="flex flex-col">
                                            <p>Lorem adipisicing </p>
                                            <p>Lorem adipisicing </p>
                                        </div>
                                    </div>
                                    <button class="font-bold text-left">Buy Now</button>

                                </div>

                                
                            </div>
                        </div>
                    </div>
                </div>
                </div>

            </div>
        </div> 
        
        <div class="zone10 bg-gray-200 flex flex-col space-y-4 items-center text-center py-8">
            <div class="font-bold text-2xl">
                Our Sponsors
            </div>
            <div class="">Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere delectus cum consectetur praesentium nihil ex expedita quod molestias labore.</div>
            <div class="flex flex-row space-x-4">
                <div class="h-12 w-24 card"></div>
                <div class="h-12 w-24 card"></div>
                <div class="h-12 w-24 card"></div>
                <div class="h-12 w-24 card"></div>
                <div class="h-12 w-24 card"></div>
                <div class="h-12 w-24 card"></div>
            </div>  
        </div>
        
    </div>
